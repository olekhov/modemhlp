# English


# Russian

Набор скриптов для общения с USB-модемом.
У меня модель ID 1bbb:0000 T & A Mobile Phones

Фрагмент из dmesg:

	[712940.720499] usb 2-1.5: New USB device found, idVendor=1bbb, idProduct=0000
	[712940.720502] usb 2-1.5: New USB device strings: Mfr=3, Product=2, SerialNumber=4
	[712940.720504] usb 2-1.5: Product: HSPA Data Card
	[712940.720505] usb 2-1.5: Manufacturer: USBModem
	[712940.720506] usb 2-1.5: SerialNumber: 1234567890ABCDEF

Опознаётся как три /dev/ttyUSB[0-2] плюс картридер /dev/sde

Для работы требуется dos2unix
http://www.xs4all.nl/~waterlan/dos2unix.html http://sourceforge.net/projects/dos2unix/